from msilib.schema import Directory
import sys
import re

# DEFの行
#1、シンボル名(部品名)
#2、プレフィックス、存在しない場合は「〜」
#3、常にゼロ
#4、ピンの終点からのミル単位のピン名オフセットです。 ゼロに設定すると、ピン名は形状の外側に設定
#5、ピン番号表示「Y or N」
#6、ピン名表示「Y or N」
#7、シンボル内の「パーツ」の数を示します。
#7、その他はオプションです。
#8、パーツのロック「L：ロック、F：アンロック」
#9、P：電源、N：それ以外

#F0
#
#F1
#F2:フットプリント名
#
#


#####################################################
# シンボル１個分のクラス
#####################################################
class EESchemaSymbol:
    def __init__(self,EESchemaText):
        #クラス変数
        self.OrgText    = EESchemaText
        self.name       = re.findall(r'^DEF\s.*\s',EESchemaText)
    def __str__(self):
        return "Symbol : " + self.name 
#####################################################
# KiCADライブラリ（複数シンボル）のクラス
#####################################################
class KiCadLib:
    #コンストラクタ：ファイルオープンしてデータ読み込み
    def __init__(self,LibFilePath):
        #インスタンス変数
        self._FilePath      = LibFilePath
        self._Symboles      = []
        
        #ファイル読み込み
        try:
            f = open(self._FilePath, 'r')
        except IOError:
            print("can not open file : %s",self._FilePath)
            sys.exit()
        else:
            sss = re.findall(r'DEF.*?ENDDEF', f.read(), flags=re.DOTALL) #DEFからENDDEFまでに切ったシンボルのリストにする
            for s in sss:
                self._Symboles.append(EESchemaSymbol(s))
            f.close()
        #
        for sym in self._Symboles:
            print("**********************************")
            print(sym)

    #     #シンボル分割
    #     symbols = []
    #     detect_symbol = False
    #     for line_text in self._EESchemaText:
    #         sp = line_text.split()
    #         if sp != []:
    #             if sp[0] == "DEF":
    #                 detect_symbol = True
    #                 temp_symbol = EESchemaSymbol(sp[1])
    #             elif sp[0] == "ENDDEF":
    #                 detect_symbol = False
    #                 symbols.append(temp_symbol)
    #                 del temp_symbol
    #             #
    #             if detect_symbol==True:
    #                 pass
    #     for sym in symbols:
    #         print(sym)
    # #読み込みデータの表示
    # def show(self):
    #     print("lib show")
    #     print(self._EESchemaText)

#------------------------------------------------
def main():
    lib = KiCadLib('../Symbole/Lib-PAD.lib')


if __name__ == "__main__":
    main()